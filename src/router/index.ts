import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: {
        default: HomeView,
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/about',
      name: 'about',
      component: {
        default: () => import('../views/AboutView.vue'),
        menu: () => import('@/components/menus/AboutMenu.vue'),
        header: () => import('@/components/headers/AboutHeader.vue')
      },
      meta: {
        layout: 'FullLayout'
      }
    },
    {
      path: '/product',
      name: 'product',
      component: {
        default: () => import('../views/products/ProductView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue')
      },
      meta: {
        layout: 'MainLayout'
      }
    },
    {
      path: '/product/full',
      name: 'product fullscreen',
      component: {
        default: () => import('../views/products/ProductView.vue'),
        menu: () => import('@/components/menus/MainMenu.vue'),
        header: () => import('@/components/headers/MainHeader.vue')
      },
      meta: {
        layout: 'FullLayout'
      }
    }
  ]
})

export default router
